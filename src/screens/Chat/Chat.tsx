import React, { FormEvent } from "react";
import { useParams } from "react-router";
import { Dialogs, Messages, Chat as ChatModel } from "@yeobill/react-chat";

import Input from "components/Input/Input";

const Chat: React.FC = () => {
  const { id } = useParams();

  const [isLoading, setLoadingState] = React.useState(true);

  React.useEffect(() => {
    Dialogs.getHistory(String(id)).then(() => setLoadingState(false));
  }, [id]);

  const { messages, sendMessage } = Messages.useMessages(String(id));
  const [newMessage, setNewMessage] = React.useState("");

  const { session } = ChatModel.useChat();

  if (!session) {
    return <div>You need login</div>;
  }

  const { user_id: userId } = session;

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();

    if (!newMessage) {
      return;
    }

    sendMessage({ message: newMessage });
    setNewMessage("");
  };

  return (
    <>
      <div>Chat</div>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        messages.map(message => (
          <div key={message._id}>
            [{message.sender_id === userId ? "outgoing" : "incoming"}]:
            {message.message}
          </div>
        ))
      )}
      <form onSubmit={handleSubmit}>
        <Input
          placeholder="message"
          onChange={setNewMessage}
          type="text"
          value={newMessage}
        />
        <button type="submit">send</button>
      </form>
    </>
  );
};

export default Chat;
