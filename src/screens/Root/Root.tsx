import React from "react";

const Root: React.FC = () => (
  <>
    <h1>Intro</h1>
    <div>Simple app with chat</div>
  </>
);

export default Root;
