import React from "react";
import { Link } from "react-router-dom";
import { Dialogs } from "@yeobill/react-chat";

const DialogsList: React.FC = () => {
  const [isLoading, setLoadingState] = React.useState(true);
  React.useEffect(() => {
    Dialogs.get().then(() => setLoadingState(false));
  }, []);

  const dialogs = Dialogs.useAll();

  return (
    <>
      <div>Dialogs</div>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        Object.values(dialogs).map(dialog => (
          <Link to={`/dialogs/${dialog._id}`} key={dialog._id}>
            {dialog.name}
          </Link>
        ))
      )}
    </>
  );
};

export default DialogsList;
