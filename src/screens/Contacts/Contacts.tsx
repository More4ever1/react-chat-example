import React, { FormEvent } from "react";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { Contacts, Users, Dialogs } from "@yeobill/react-chat";

import Input from "components/Input/Input";

const ContactsList: React.FC = () => {
  const [isLoading, setLoadingState] = React.useState(true);
  React.useEffect(() => {
    Promise.all([Contacts.get(), Dialogs.get()]).then(() =>
      setLoadingState(false)
    );
  }, []);

  const [login, setLogin] = React.useState("");
  const history = useHistory();

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    if (!login) {
      return;
    }

    const user = await Users.findByLogin(login);
    if (!user) {
      return;
    }

    const { _id: id } = await Dialogs.create(user.id);
    history.push(`/dialogs/${id}`);
  };

  const contacts = Contacts.useAll();

  return (
    <>
      <h1>Contacts</h1>
      <form onSubmit={handleSubmit}>
        <Input
          placeholder="login"
          onChange={setLogin}
          type="text"
          value={login}
        />
        <button type="submit">search</button>
      </form>

      {isLoading ? (
        <div>Loading...</div>
      ) : (
        Object.values(contacts).map(contact => (
          <Link to={`/dialogs/${contact.dialogId}`} key={contact.id}>
            {contact.login}
          </Link>
        ))
      )}
    </>
  );
};

export default ContactsList;
