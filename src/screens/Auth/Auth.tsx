import React from "react";
import { useHistory } from "react-router";

import Input from "components/Input/Input";
import initChatByCreds from "../../helpers/initChatByCreds";
import AuthService from "../../services/AuthService";


const Auth: React.FC = () => {
  const [isLoading, setLoadingState] = React.useState(false);
  const [error, setError] = React.useState("");
  const [login, setLogin] = React.useState("");
  const [password, setPassword] = React.useState("");
  const history = useHistory();

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    if (!login || !password) {
      return;
    }

    setError("");
    setLoadingState(true);

    try {
      await initChatByCreds(login, password);
    } catch (error) {
      setLoadingState(false);
      setError(error.message);
      return;
    }
    AuthService.creds = {login, password};

    history.push("/dialogs");
  };

  return (
    <>
      <h1>Auth</h1>
      <div>{error}</div>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        <form onSubmit={handleSubmit}>
          <p>
            <Input
              placeholder="login"
              type="text"
              onChange={setLogin}
              value={login}
            />
          </p>
          <p>
            <Input
              placeholder="password"
              type="password"
              onChange={setPassword}
              value={password}
            />
          </p>
          <button type="submit">log in</button>
        </form>
      )}
    </>
  );
};

export default Auth;
