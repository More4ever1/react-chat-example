/**
 * This is a simple authenticate realization.
 * Don't use it in production.
 */

import { HmacSHA1 } from "crypto-js";
import { stringify } from "qs";
import { TSession } from "@yeobill/react-chat/lib/types";

type TUserForLogin = {
  [k: string]: string;
  login: string;
  password: string;
};

type TCredentials = {
  appId: string;
  authKey: string;
  authSecret: string;
};

interface TypedResponse<T = any> extends Response {
  json<P = T>(): Promise<P>
}

declare function fetch<T>(...args: any): Promise<TypedResponse<T>>;

const getSession = ({ appId, authKey, authSecret }: TCredentials) => (
  user: TUserForLogin
) => {
  const message = {
    application_id: appId,
    auth_key: authKey,
    nonce: Math.floor(Math.random() * 10000),
    timestamp: +(+new Date() / 1000).toFixed(),
    user
  };

  const signature = HmacSHA1(
    stringify(message, { encode: false }),
    authSecret
  ).toString();

  return fetch<{session: TSession}>("https://api.quickblox.com/session.json", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "QuickBlox-REST-API-Version": "0.1.1"
    },
    body: JSON.stringify({
      ...message,
      signature
    })
  }).then(resp => resp.json()).then(({session}) => session);
};

export { getSession };
