import { Chat } from '@yeobill/react-chat';
import {getSession} from "../screens/Auth/getSession";

const CHAT_CREDENTIAL = {
  appId: process.env.REACT_APP_CHAT_APP_ID || '',
  authKey: process.env.REACT_APP_CHAT_KEY || '',
  authSecret: process.env.REACT_APP_CHAT_SECRET || '',
};

const loginWithBackendSim = getSession(CHAT_CREDENTIAL);


const initChatByCreds = async (login: string, password: string) => {
  const session = await loginWithBackendSim({
    login,
    password
  });


  if (!session) {
    throw new Error('login or password incorrect');
  }

  await Chat.initChat({
    ...CHAT_CREDENTIAL,
    session
  });
};

export default initChatByCreds;
