const credsKey = 'sessionCreds';

const AuthService = {
  set creds(creds){
    localStorage.setItem(credsKey, JSON.stringify(creds))
  },
  get creds(){
    const credsString = localStorage.getItem(credsKey);

    if(!credsString){
      return undefined
    }

    return JSON.parse(credsString);
  },
};

export default AuthService;
