import React, {PureComponent} from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  RouteProps
} from "react-router-dom";
import { Chat as ChatController } from "@yeobill/react-chat";

import Auth from "screens/Auth/Auth";
import Chat from "screens/Chat/Chat";
import Contacts from "screens/Contacts/Contacts";
import Dialogs from "screens/Dialogs/Dialogs";
import Root from "./screens/Root/Root";
import initChatByCreds from "./helpers/initChatByCreds";
import AuthService from "./services/AuthService";

const ChatRoute = ({ children, ...rest }: RouteProps) => {
  const { session } = ChatController.useChat();

  return (
    <Route
      {...rest}
      render={({ location }) =>
        session ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
};

const Nav: React.FC = () => {
  const { session } = ChatController.useChat();
  return (
    <nav>
      <ul>
        {session ? (
          <>
            <li>
              <Link to="/contacts">Contacts</Link>
            </li>
            <li>
              <Link to="/dialogs">Dialogs</Link>
            </li>
          </>
        ) : (
          <li>
            <Link to="/login">Auth</Link>
          </li>
        )}
      </ul>
    </nav>
  );
};

const initialState = {
    inited: false,
};

class App extends PureComponent{
    state = {...initialState};

    componentDidMount(): void {
        (async () => {
            const creds = AuthService.creds;

            if (creds) {
                const {login, password} = creds;

                try {
                    await initChatByCreds(login, password)
                } catch (error) {
                    console.error('auth resttore error:', error.message);
                }
            }

            this.setState({inited: true})

        })();
    }

    render(){
        const {inited} = this.state;

        if (!inited) {
            return 'LOADING....'
        }

  return (
    <Router>
      <>
        <Nav />
        <Switch>
          <ChatRoute path="/contacts">
            <Contacts />
          </ChatRoute>
          <ChatRoute path="/dialogs/:id">
            <Chat />
          </ChatRoute>
          <ChatRoute path="/dialogs">
            <Dialogs />
          </ChatRoute>
          <Route path="/login">
            <Auth />
          </Route>
          <Route path="/">
            <Root />
          </Route>
        </Switch>
      </>
    </Router>
  );
}
}

export default App;
