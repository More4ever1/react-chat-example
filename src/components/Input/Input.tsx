import React from "react";

type TProps = {
  onChange: (newValue: string) => void;
  placeholder?: string;
  type: "text" | "password";
  value: string;
};

const Input: React.FC<TProps> = ({ onChange, placeholder, type, value }) => {
  return (
    <input
      placeholder={placeholder}
      onChange={({ target: { value } }) => onChange(value)}
      type={type}
      value={value}
    />
  );
};

export default Input;
